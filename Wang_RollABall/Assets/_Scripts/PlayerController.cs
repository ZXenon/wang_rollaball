﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float jumpPower;
    bool onGround;
	public Text countText;
	public Text winText;
    Vector3 origin;
    Rigidbody rb;
	int count;

    float timePassed = 0f;
    int totalPickups;
    public GameObject pickupPrefab;
    // Start is called before the first frame update
    void Start()
    {
        rb= GetComponent<Rigidbody>();
        count = 0;
		
		winText.text = "";

        GameObject[] allPickups = GameObject.FindGameObjectsWithTag("Pick Up");
        totalPickups = allPickups.Length;
        print(totalPickups);
        SetCountText();
        origin = transform.position;
        // start position is recorded when game starts
    }

    void Heightcheck ()
    {
        if (transform.position.y < 0)
        {
            transform.position = origin;
            // ball is returned to its spawn position when moved out of border
        }
    }
    private void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .55f);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }

        timePassed += Time.deltaTime;
        print(timePassed);
        Heightcheck();
    }

    void Jump()
    {
        //add upward force
        rb.AddForce(Vector3.up * jumpPower);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement*speed);
	}
	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.CompareTag("Pick Up"))
		{
			other.gameObject.SetActive (false);
			count = count + 1;
			SetCountText ();
            SpeedIncrease();
		}
	}
    void SpeedIncrease()
    {
        speed = speed + 2;
      
    }
    // speed will increase by 2 for each pickup item being picked up
	void SetCountText ()
	{
		countText.text ="Count: " + count.ToString ();
		if (count >= 12)
		{
			winText.text = "You Win!";
		}
	}

    void SpawnRandomPickup()
    {
        Vector3 randomPosition = new Vector3(Random.Range(-5f, 5f), .5f, Random.Range(-5f, 5f));
        Instantiate(pickupPrefab,randomPosition,pickupPrefab.transform.rotation);
        totalPickups++;
    }
}
